import os
import sys
from dotenv import load_dotenv
import mysql.connector as msql 
from mysql.connector import Error
import csv
import pickle
from datetime import datetime
import pandas
import json
import time

CLEAN_RECIPES = 'clean_recipes.csv'
RAW_RECIPES = 'RAW_recipes.csv'
PP_RECIPES = 'PP_recipes.csv'
RAW_INTERACTIONS = 'RAW_interactions.csv'
REVIEWS = 'reviews.csv'
INGREDIENT_MAP = 'ingr_map.pkl'

try:
    load_dotenv()
    HOSTNAME = os.getenv('HOSTNAME')
    DATABASE = os.getenv('DATABASE')
    USERNAME = os.getenv('USERNAME')
    PASSWORD = os.getenv('PASSWORD')

    print(f"Connecting to {DATABASE}...")
    connection = msql.connect(
        host     = HOSTNAME,
        user     = USERNAME,
        password = PASSWORD,
        database = DATABASE
    )
    cursor = connection.cursor()
except Error as e:
    print('Error while connecting to MySQL', e)
    sys.exit()

print(f"Connected to {DATABASE} successfully!")

reviewsFile = open(REVIEWS, newline='')
reviewsReader = csv.DictReader(reviewsFile, delimiter=';')

rawInteractionsFile = open(RAW_INTERACTIONS, newline='', encoding='utf8')
rawInteractionsReader = csv.DictReader(rawInteractionsFile, delimiter=',')

cleanRecipesFile = open(CLEAN_RECIPES, newline='')
cleanRecipesReader = csv.DictReader(cleanRecipesFile, delimiter=';')

rawRecipesFile = open(RAW_RECIPES, newline='', encoding='utf8')
rawRecipesReader = csv.DictReader(rawRecipesFile, delimiter=',')

PPRecipesFile = open(PP_RECIPES, newline='')
PPRecipesReader = csv.DictReader(PPRecipesFile, delimiter=',')

###############
# INGREDIENTS #
###############

def insert_ingredients():
    add_ingredient = (
        "INSERT INTO Ingredients"
        "(ingredientID, ingredientName)"
        "VALUES (%s, %s)"
    )

    # unpickle ingr_map.pkl, use set to map PP_recipes to ingredient names and IDs
    print("loading ingredients...")
    ingredientFile = open(INGREDIENT_MAP, 'rb')
    ingredientDictionary = pickle.load(ingredientFile)
    ing_set = {}
    for i, row in ingredientDictionary.iterrows():
        if row['id'] not in ing_set:
            ing_name = row['replaced'].lower()
            ing_set[row['id']] = ing_name
            ingredientData = (row['id'], ing_name)
            cursor.execute(add_ingredient, ingredientData)
    connection.commit()

    # go through clean_recipes.csv, for each ingredient column in row, parse each ingredient
    # check if ingredient already exists (by name) in table from earlier dataset, if not, enter it with generated id

    print("loading extra ingredients...")
    cursor.execute("SELECT MAX(ingredientID) FROM Ingredients")
    extraIngredientID = cursor.fetchone()[0]
    # print(extraIngredientID)

    # check_ingredient = ('SELECT ingredientID FROM Ingredients '
    #                     'WHERE ingredientName = %s')

    for row in cleanRecipesReader:
        ingredients = row['Ingredients'].split(',')
        for i in range(len(ingredients)):
            ingredientName = ingredients[i].strip()
            if ingredientName != '':
                # print("statement:", check_ingredient)
                # print("param:    ", ingredientName)
                # cursor.execute(check_ingredient, (ingredientName,))
                # cursor.fetchone()
                # if (cursor.rowcount == 0):
                if ingredientName.lower() not in ing_set.values():
                    # print(row['Ingredients'])
                    extraIngredientID += 1
                    ingredientData = (extraIngredientID, ingredientName)
                    # print("data:", ingredientData)
                    cursor.execute(add_ingredient, ingredientData)
                    ing_set[extraIngredientID] = ingredientName.lower()
            
    connection.commit()

#########
# USERS #
#########
def insert_users():
    print("loading users...")
    users_set = set()
    add_user = ('INSERT INTO Users '
                '(userID)'
                'VALUES (%s)')

    # insert all profileIDs from reviews.csv with a '1' prefix 
    print("reading 'reviews.csv'...")
    for row in reviewsReader:
        if row['profileID'] is not None:
            if row['profileID'].isnumeric():
                userID = '1' + row['profileID']
                if userID not in users_set:
                    cursor.execute(add_user, (userID,))
                    users_set.add(userID)
    connection.commit()

    # insert all user_ids from RAW_interactions.csv with a '2' prefix
    print("reading 'RAW_interactions.csv'...")
    for row in reviewsReader:
        if row['user_id'] is not None:
            if(row['user_id'].isnumeric()):
                userID = '2' + row['user_id']
                if userID not in users_set:
                    cursor.execute(add_user, userID)
                users_set.add(userID)

    connection.commit()

###########
# RECIPES #
#  Uses   #
###########
def insert_clean_recipes():
    print("loading 'clean_recipes.csv'...")
    add_recipe = ('INSERT INTO Recipes '
                '(recipeID, recipeName, authorName, minutesToComplete, dateSubmitted, photo, directions)'
                'VALUES (%s, %s, %s, %s, %s, %s, %s)')

    add_use = ('INSERT INTO Uses '
                '(recipeID, ingredientID)'
                'VALUES (%s, %s)')

    # insert '1' + RecipeID, Recipe Name, Recipe Photo, Author, Total Time, null, Directions - from clean_recipes.csv 

    check_ingredient = ('SELECT ingredientID FROM Ingredients '
                        'WHERE ingredientName = %s')

    for row in cleanRecipesReader:
        if row['RecipeID'] is not None:
            if(row['RecipeID'].isnumeric()):
                minutesString = row['Total Time'].split(' ')
                totalMinutes = 0
                for i in range(len(minutesString)//2):
                    if minutesString[2*i + 1] == 'd':
                        totalMinutes += 1440 * int(minutesString[2*i])
                    elif minutesString[2*i + 1] == 'h':
                        totalMinutes += 60 * int(minutesString[2*i])
                    elif minutesString[2*i + 1] == 'm':
                        totalMinutes += int(minutesString[2*i])
                if totalMinutes <= 0:
                    totalMinutes = None
                
                recipeData = ('1' + row['RecipeID'], row['Recipe Name'], row['Author'], totalMinutes, None, row['Recipe Photo'], row['Directions'])
                cursor.execute(add_recipe, recipeData)

                # after recipe is inserted, go through each ingredient in ingredient column
                # add entries to Uses: search for each ID of ingredient name in Ingredients, use that and RecipeID
                ingredients = row['Ingredients']
                ingredients = ingredients.split(',')
                ing_set = set()
                for i in range(len(ingredients)):
                    ingredientName = ingredients[i]
                    if ingredientName != '':
                        cursor.execute(check_ingredient, (ingredientName,))
                        result = cursor.fetchone()
                        if result is not None:
                            if result[0] not in ing_set:
                                ing_set.add(result[0])
                                useData = ('1' + row['RecipeID'], result[0])
                                cursor.execute(add_use, useData)
    connection.commit()

def insert_raw_recipes():
    # insert '2' + id, name, '', '', minutes, submitted, steps* - from RAW_recipes.csv
    print("loading 'RAW_recipes.csv'...")
    add_recipe = ('INSERT INTO Recipes '
                '(recipeID, recipeName, authorName, minutesToComplete, dateSubmitted, photo, directions)'
                'VALUES (%s, %s, %s, %s, %s, %s, %s)')
   
    for row in rawRecipesReader:
        if(row['id'].isnumeric()):
            # *parse steps: array of '' strings -> one string with ''. ''. '' 
            unparsedSteps = row['steps']
            unparsedSteps = unparsedSteps.replace('[', '')
            unparsedSteps = unparsedSteps.replace(']', '')
            directions = ''
            while(unparsedSteps is not None and unparsedSteps.find('\'') != -1):
                firstQuotation = unparsedSteps.find('\'')
                unparsedSteps = unparsedSteps[firstQuotation + 1:]
                secondQuotation = unparsedSteps.find('\'')
                directions += unparsedSteps[:secondQuotation] + '. '
                unparsedSteps = unparsedSteps[secondQuotation + 1:]

            date = datetime.strptime(row['submitted'], "%Y-%m-%d")
            if row['minutes'] is not None and row['minutes'].isnumeric():
                minutes_to_cook = int(row['minutes'])
                if len(row['minutes']) > 6 or minutes_to_cook == 0:
                    minutes_to_cook = None
            else:
                minutes_to_cook = None

            recipeData = ('2' + row['id'], row['name'], '', minutes_to_cook, date, None, directions)
            cursor.execute(add_recipe, recipeData)
    connection.commit()

def insert_pp_recipes():
    # go through PP_recipes, add entry to Uses using id, and each ingredient_id in ingredient_ids column
    print("loading 'PP_recipes.csv'...")
    add_use = ('INSERT INTO Uses '
                '(recipeID, ingredientID)'
                'VALUES (%s, %s)')

    for row in PPRecipesReader:
        if row['id'] is not None and row['id'].isnumeric():
            ingredientIDs = set(json.loads(row['ingredient_ids']))
            for i in ingredientIDs:
                useData = (int('2' + row['id']), i)

                try:
                    cursor.execute(add_use, useData)
                except:
                    pass
                    # some ingredient IDs for some reason do not exist in the Ingredients table
                    # foreign key constraint failure
                    # just don't include it because we don't know what ingredient
                    # it's supposed to correspond to
    connection.commit()

################
# INTERACTIONS #
#   Reviews    #
#   Writes     #
################
def insert_interactions_reviews():
    add_interaction = ('INSERT INTO Interactions '
                '(interactionID, comment)'
                'VALUES (%s, %s)')

    add_review = ('INSERT INTO Reviews '
                '(interactionID, recipeID, rating)'
                'VALUES (%s, %s, %s)')

    add_write = ('INSERT INTO Writes '
                '(interactionID, userID)'
                'VALUES (%s, %s)')
    
    select_recipe = (
        'SELECT * FROM Recipes WHERE recipeID = %s'
    )

    select_user = (
        'SELECT * FROM Users WHERE userID = %s'
    )
    # keep track of interactionID of interaction being inserted
    interactionID = 1

    # insert '1' + profileID, rate, comment - from reviews.csv
    # insert into Reviews: use interactionID, '1' + recipeID, rate 
    # insert into Writes: use interactionID, '1' + profileID

    for row in reviewsReader:
        if row is None:
            pass
        elif row['profileID'] is not None and row['profileID'].isnumeric():
            recipe_id = int('1' + row['RecipeID'])
            profile_id = int('1' + row['profileID'])

            cursor.execute(select_recipe, (recipe_id,))
            if cursor.fetchone() is None:
                continue
            cursor.execute(select_user, (profile_id,))
            if cursor.fetchone() is None:
                continue
            
            interactionData = (interactionID, row['Comment'])
            reviewData = (interactionID, recipe_id, int(row['Rate']))
            writeData = (interactionID, profile_id)

            cursor.execute(add_interaction, interactionData)
            cursor.execute(add_review, reviewData)
            cursor.execute(add_write, writeData)

            interactionID += 1

    connection.commit()

def insert_raw_interactions():
    print("inserting 'RAW_interactions.csv'...")
    # insert '2' + user_id, rating, review - from RAW_interactions.csv
    # insert into Reviews: use interactionID, '2' + recipe_id, rating
    # insert into Writes: use interactionID, '2' + user_id
    add_interaction = ('INSERT INTO Interactions '
                '(interactionID, comment)'
                'VALUES (%s, %s)')

    add_review = ('INSERT INTO Reviews '
                '(interactionID, recipeID, rating)'
                'VALUES (%s, %s, %s)')

    add_write = ('INSERT INTO Writes '
                '(interactionID, userID)'
                'VALUES (%s, %s)')
    
    select_recipe = (
        'SELECT * FROM Recipes WHERE recipeID = %s'
    )

    select_user = (
        'SELECT * FROM Users WHERE userID = %s'
    )

    max_interaction_id = (
        'SELECT MAX(interactionID) FROM Interactions'
    )

    cursor.execute(max_interaction_id)
    interactionID = cursor.fetchone()[0]

    # total_rows = len(list(rawInteractionsReader))
    # print(total_rows)
    # start_time = time.time()
    # print(start_time)
    for row in rawInteractionsReader:
        # print(row)
        # if counter == 100:
        #     duration = time.time() - start_time
        #     print(f"estimated {duration * total_rows/100} seconds")
        # else:
        #     counter += 1

        if row is None:
            pass
        elif row['user_id'].isnumeric():
            recipe_id = '2' + row['recipe_id']
            profile_id = '2' + row['user_id']

            cursor.execute(select_recipe, (recipe_id,))
            if cursor.fetchone() is None:
                continue
            cursor.execute(select_user, (profile_id,))
            if cursor.fetchone() is None:
                continue

            interactionData = (interactionID, row['review'])
            reviewData = (interactionID, '2' + row['recipe_id'], row['rating'])
            writeData = (interactionID, '2' + row['user_id'])

            cursor.execute(add_interaction, interactionData)
            cursor.execute(add_review, reviewData)
            cursor.execute(add_write, writeData)

            interactionID += 1

    connection.commit()

if __name__ == "__main__":
    # insert_ingredients()
    # insert_users()
    # insert_clean_recipes()
    # insert_raw_recipes()
    # insert_pp_recipes()
    # insert_interactions_reviews()
    insert_raw_interactions()
    print("Done!")
    cursor.close()
    connection.close()