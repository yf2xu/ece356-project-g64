CREATE VIEW Target AS SELECT recipeID FROM Recipes;

-- search for recipe by name
ALTER VIEW Target AS 
	SELECT recipeID FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID) 
	WHERE recipeName LIKE '%userInput%';

-- search for recipe by ingredient
ALTER VIEW Target AS 
	SELECT recipeID  
		FROM ((Uses INNER JOIN Target ON Uses.recipeID = Target.recipeID) 
		INNER JOIN Ingredients ON Uses.ingredientID = Ingredients.ingredientID) 
		WHERE IngredientName LIKE '%userinput%';

-- search for recipe by author
ALTER VIEW Target AS 
	SELECT recipeID FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID) 
	WHERE authorName LIKE '%userInput%';

-- search for recipe by # minutes
ALTER VIEW Target AS 
	SELECT recipeID FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID) 
	WHERE minutesToComplete < userInput;
ALTER VIEW Target AS 
	SELECT recipeID FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID)  
	WHERE minutesToComplete > userInput;

-- sort recipe by # reviews
ALTER VIEW Target AS 
	SELECT recipeID, COUNT(recipeID) AS reviewCount
	FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID
	GROUP BY recipeID
	ORDER BY reviewCount ASC;
ALTER VIEW Target AS 
	SELECT recipeID, COUNT(recipeID) AS reviewCount
	FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID
	GROUP BY recipeID
	ORDER BY reviewCount DESC;

-- sort recipe by rating
ALTER VIEW Target AS 
	SELECT recipeID, AVG(rating) AS avgRating
	FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID
	GROUP BY recipeID
	ORDER BY avgRating ASC;
ALTER VIEW Target AS 
	SELECT recipeID, AVG(rating) AS avgRating
	FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID
	GROUP BY recipeID
	ORDER BY avgRating DESC;
	
-- show reviews
SELECT recipeID, comment
	FROM ((Reviews INNER JOIN Target ON Reviews.recipeID = Target.recipeID)
	INNER JOIN Interactions ON Reviews.interactionID = Interactions.interactionID)

-- restricted to only the ingredients given by the user (ie. don’t show recipes that include other ingredients)
ALTER VIEW Target AS 
	SELECT recipeID  
		FROM ((Uses INNER JOIN Target ON Uses.recipeID = Target.recipeID) 
		INNER JOIN Ingredients ON Uses.ingredientID = Ingredients.ingredientID) 
		WHERE IngredientName LIKE '%userinput1%' AND
		IngredientName LIKE '%userinput2%';
		
-- create a recipe
INSERT INTO Recipes
	VALUES (userInput);

-- create a review for a recipe
INSERT INTO Interactions
	VALUES (userInput);
INSERT INTO Reviews
	VALUES (userInput);
INSERT INTO Writes
	VALUES (userInput);

-- Editing a recipe
UPDATE Recipes
	WHERE recipeID = userInput
	SET userInput;

-- Editing a review
UPDATE Interactions
	WHERE interactionID = userInput
	SET userInput;

-- Deleting a recipe
DELETE FROM Recipes
	WHERE recipeID = userInput;

-- Deleting a review
DELETE FROM Interactions
	WHERE InteractionID = userInput;
DELETE FROM Reviews
	WHERE InteractionID = userInput;
DELETE FROM Writes
	WHERE InteractionID = userInput;


SELECT recipeID, rating, comment
	FROM Reviews INNER JOIN Interactions
	ON Reviews.interactionID = Interactions.InteractionID
	WHERE recipeID = 17000
	LIMIT 0, 10;

SELECT recipeName, AVG(rating) AS avgRating 
	FROM Reviews 
	INNER JOIN Recipes
	on Reviews.recipeID = Recipes.recipeID
	LIMIT 5;

WITH ValidRecipeIDs AS (SELECT recipeID 
	FROM Ingredients INNER JOIN Uses
	ON Uses.ingredientID = Ingredients.ingredientID
	WHERE ingredientName = 'macadamia')
SELECT *
	FROM ValidRecipeIDs
	INNER JOIN Reviews USING (recipeID)
	LIMIT 2;

SELECT recipeName, authorName, minutesToComplete, AVG(rating) as avgRating
	FROM Ingredients 
	INNER JOIN Uses
	ON Uses.ingredientID = Ingredients.ingredientID
	INNER JOIN Recipes
	on Uses.recipeID = Recipes.recipeID
	INNER JOIN Reviews
	on Recipes.recipeID = Reviews.recipeID
	WHERE ingredientName = 'macadamia'
	LIMIT 10;

SELECT recipeName, AVG(rating) AS avgRating
	FROM Recipes INNER JOIN Reviews USING (recipeID)
	
	WHERE recipeID = 17255;

WITH AvgRatings as (SELECT recipeID, AVG(rating) as avgRating
	FROM Reviews
	GROUP BY recipeID)
SELECT *
	FROM Ingredients
	INNER JOIN Uses ON Uses.ingredientID = Ingredients.ingredientID
	INNER JOIN AvgRatings ON AvgRatings.recipeID = Uses.recipeID
	INNER JOIN Recipes ON Recipes.recipeID = AvgRatings.recipeID
	LIMIT 10;
