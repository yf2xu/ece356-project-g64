# ECE 356 Project

[Project Demo](https://drive.google.com/file/d/1Po2NopuTVthXY2NTCyL-0FKwxnZbgu3Q/view?usp=sharing)

## CLI Files
* **client.py**: Contains the client app, which interfaces with the user and execute commands via importing Commands from **commands.py**
* **commands.py**: Contains the logic and queries for all commands

## Other Files
* **README.md**: What you're reading right now
* **\*.csv**, **ingr_map.pkl**: Data files used for the database
* **loadTables.py**: Script used to genereate entries to the database from the data files
* **create.sql, queries.sql**: Contains mysql queries 

## CLI Commands
<details><summary>Create recipe</summary>

* Creates a recipe from given arguments
* argument(s): `recipe_name, author_name (optional), minutes_to_complete, date_submitted (defaults to now), photo_url (optional), ingredients, directions`
* output on valid arguments: Creates a new recipe entry and inserts it into the `Recipes` table

</details>

<details><summary>Delete recipe</summary>

* Delete a recipe given a specific recipeID
* argument(s): `recipe_id`
* output on valid arguments: Removes the entry with the same recipeID from the `Recipes` table

</details>

<details><summary>Edit recipe</summary>

* Edit a recipe from given arguments
* argument(s): `recipe_id, recipe_name, author_name (optional), minutes_to_complete, photo_url (optional), directions`
* output on valid arguments: Updates the entry with the same recipeID with new values given in the command

</details>

<details><summary>Create Review</summary>

* Creates a review from given arguments
* argument(s): `recipe_ikd, user_id, rating, comment`
* output on valid arguments: Creates a new review entry and inserts relevant data into appropriate tables

</details>

<details><summary>Delete review</summary>

* Deletes a review given a specific interactionID
* argument(s): `interaction_id`
* output on valid arguments: Removes the entryh with the same interactionID from appropriate tables

</details>

<details><summary>Edit review</summary>

* Edit a review from given arguments
* argument(s): `interaction_id, rating, comment`
* output on valid arguments: Updates entries with the same interactionID with new values given in the command

</details>

<details><summary>Show reviews for recipe</summary>

* Displays reviews for a specific recipe
* argument(s): `recipe_id, start`
* output on valid arguments: Display 10 reviews for the recipe matching the given recipeID, starting from the review given by the start index

</details>

<details><summary>Search Recipe</summary>

* Filters recipes with specific attributes
* argument(s): `recipe_name (optional), author_name (optional), max_time (optional)`
* output on valid arguments: Updates the table used for displaying search results to only recipes that fit the given criteria (criterion)

</details>

<details><summary>Display Recipe Details</summary>

* Display recipe details for a given recipeID
* argument(s): `recipe_id`
* output on valid arguments: Prints out recipe details for a speficied recipeID

</details>
