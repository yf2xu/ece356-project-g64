import os
import time
import mysql.connector as msql
from mysql.connector import Error
from dotenv import load_dotenv
from datetime import datetime

# Globals
load_dotenv()
HOSTNAME = os.getenv('HOSTNAME')
DATABASE = os.getenv('DATABASE')
USERNAME = os.getenv('USERNAME')
PASSWORD = os.getenv('PASSWORD')
connection = msql.connect(
    host     = HOSTNAME,
    user     = USERNAME,
    password = PASSWORD,
    database = DATABASE
)
cursor = connection.cursor()

# initializes the table for searching
cursor.execute("DROP TABLE IF EXISTS Target")
connection.commit()
cursor.execute("CREATE TABLE Target AS SELECT recipeID, recipeName, minutesToComplete FROM Recipes")
connection.commit()

# globals for result listings
results_per_page = 10
index = 0
cursor.execute("SELECT * FROM Target")
results = cursor.fetchall()

def now():
    return time.strftime('%Y-%m-%d %H:%M:%S')

class InvalidInputError(Exception):
    pass

# Model for command argument
class Arg():
    def __init__(self,
        name: str,
        datatype: type,
        optional: bool=False,
        default=None):
        self.name     = name
        self.optional = optional
        self.default  = default
        self.datatype = datatype
    
    def input(self):
        # arguments will be defaulted to self.default if specified
        # otherwise let user input 
        if self.default is not None:
            return self.default()
        else:
            user_input = input(f"> {self.name}: ").strip()

        # input NULL by entering nothing
        if user_input == "":
            if not self.optional:
                print("Error: NULL given for a non-nullable argument")
                raise InvalidInputError
            else:
                return None
        else:
            try:
                user_input = self.datatype(user_input)
            except Exception as e:
                print(e)
                print(f"Error: {self.name} must be of type {self.datatype}")
                raise InvalidInputError

        return user_input

# base class for a command
class Command():
    def __init__(self, name, args):
        self.name  = name
        self.args  = args
        assert type(args) in [list, tuple]

    def get_inputs(self):
        user_inputs = {}
        for arg in self.args:
            try:
                user_inputs[arg.name] = arg.input()
            except:
                return False
        return user_inputs

# quits the program
class QuitCommand():
    def __init__(self):
        self.name = "Quit program"

    def run(self):
        print("Exiting...")
        exit()

# create a new recipe
class CreateRecipeCommand(Command):
    def __init__(self):
        self.name = "Create recipe"
        self.args = [
            Arg("recipe_name",         str),
            Arg("author_name",         str, optional=True),
            Arg("minutes_to_complete", int),
            Arg("date_submitted",      str, default=now),
            Arg("photo_url",           str, optional=True),
            Arg("ingredients",         str),
            Arg("directions",          str)
        ]

    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        # insert into Recipes table
        statement = (
            "INSERT INTO Recipes "
            "(recipeName, authorName, minutesToComplete, dateSubmitted, photo, directions) "
            "VALUES (%s, %s, %s, %s, %s, %s)"
        )
        data = (
            inputs['recipe_name'],
            inputs['author_name'],
            inputs['minutes_to_complete'],
            inputs['date_submitted'],
            inputs['photo_url'],
            inputs['directions']
        )

        cursor.execute(statement, data)
        recipe_id = cursor.lastrowid

        for ing in [x.strip().lower() for x in inputs['ingredients'].split(',')]:
            # check if ingredient already exists in Ingredients table
            statement = (
                "SELECT ingredientID FROM Ingredients "
                "WHERE ingredientName = %s"
            )
            data = (ing,)
            cursor.execute(statement, data)
            rows = cursor.fetchall()

            # ingredient does not exist yet
            #   -> insert into ingredients table
            if len(rows) == 0:
                statement = (
                    "INSERT INTO Ingredients "
                    "VALUES (%s)"
                )
                data = (ing,)
                cursor.execute(statement, data)
                ingredient_id = cursor.lastrowid
            # ingredient already exists 
            else:
                ingredient_id = rows[0][0]
            
            # insert into Uses table
            statement = (
                "INSERT INTO Uses "
                "VALUES (%s, %s)"
            )
            data = (recipe_id, ingredient_id)
            cursor.execute(statement, data)
            connection.commit()
            print(f"Inserted {cursor.rowcount} rows")

# delete a recipe
class DeleteRecipeCommand(Command):
    def __init__(self):
        self.name = "Delete recipe"
        self.args = [Arg("recipe_id", int)]

    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return
        
        statement = (
            "DELETE FROM Recipes "
            "WHERE recipeID = %s"
        )
        cursor.execute(statement, (inputs['recipe_id'],))
        connection.commit()
        print(f"Deleted {cursor.rowcount} row")

# edit a recipe
class EditRecipeCommand(Command):
    def __init__(self):
        self.name = "Edit recipe"
        self.args = [
            Arg("recipe_id", int),
            Arg("recipe_name", str),
            Arg("author_name", str, optional=True),
            Arg("minutes_to_complete", int),
            Arg("photo_url", str, optional=True),
            Arg("directions", str),
        ]

    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return
        
        statement = (
            "UPDATE Recipes "
            "SET "
            "authorName = %s, "
            "recipeName = %s, "
            "minutesToComplete = %s, "
            "photo = %s, "
            "directions = %s "
            "WHERE recipeID = %s"
        )
        data = (
            inputs['author_name'],
            inputs['recipe_name'],
            inputs['minutes_to_complete'],
            inputs['photo_url'],
            inputs['directions'],
            inputs['recipe_id'],
        )
        cursor.execute(statement, data)
        connection.commit()
        print(f"Modified {cursor.rowcount} row")

# create a review
class CreateReviewCommand(Command):
    def __init__(self):
        super().__init__(
            "Create review",
            [
                Arg("recipe_id", int),
                Arg("user_id",   str),
                Arg("rating",    int),
                Arg("comment",   str)
            ]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        cursor.execute(
            "INSERT IGNORE INTO Users VALUES (%s)",
            (inputs['user_id'],)
        )

        statement = (
            "SELECT * FROM Reviews "
            "INNER JOIN Writes on Reviews.interactionID = Writes.interactionID "
            "WHERE userID = %s AND "
            "recipeID = %s "
        )
        data = (
            inputs['user_id'],
            inputs['recipe_id']
        )
        cursor.execute(statement, data)

        if cursor.fetchone() is None:
            cursor.execute("INSERT INTO Interactions(comment) VALUES (%s)", (inputs['comment'],))
            connection.commit()
            interaction_id = cursor.lastrowid
            
            cursor.execute(
                "INSERT INTO Reviews VALUES (%s, %s, %s)",
                (interaction_id, inputs['recipe_id'], inputs['rating'])
            )
            cursor.execute(
                "INSERT INTO Writes VALUES (%s, %s)",
                (inputs['user_id'], interaction_id)
            )
            connection.commit()
            
            print(f"Successfully added new review")
        else:
            print("Error: This user already has an existing interaction on this recipe")

# delete a review
class DeleteReviewCommand(Command):
    def __init__(self):
        super().__init__(
            "Delete review",
            [Arg("interaction_id", int)]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        # delete from Interactions table
        statement = (
            "DELETE FROM Interactions "
            "WHERE interactionID = %s"
        )
        data = (inputs['interaction_id'],)
        cursor.execute(statement, data)
        connection.commit()
        print(f"Deleted {cursor.rowcount} row")

# edit a review
class EditReviewCommand(Command):
    def __init__(
        self,
        name = "Edit review",
        args = [
            Arg("interaction_id", int),
            Arg("rating", int),
            Arg("comment", str, optional=True)
        ]):
        super().__init__(name, args)
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        cursor.execute(
            "SELECT recipeID FROM Reviews WHERE interactionID = %s",
            (inputs['interaction_id'],)
        )
        result = cursor.fetchone()
        if result is None:
            print("Error: non-existent interactionID")
            return

        # insert into Interactions table
        if inputs['comment'] is not None:
            statement = (
                "UPDATE Interactions "
                "SET comment = %s "
                "WHERE interactionID = %s"
            )
            data = (
                inputs['comment'],
                inputs['interaction_id']
            )
            cursor.execute(statement, data)

        statement = (
            "UPDATE Reviews "
            "SET rating = %s "
            "WHERE interactionID = %s"
        )
        cursor.execute(
            statement,
            (inputs['rating'], inputs['interaction_id'])
        )
        connection.commit()
        print("Successfully edited review")

# search for a recipe by name
class ShowReviewsCommand(Command):
    def __init__(self):
        super().__init__(
            "Show reviews for recipe",
            [
                Arg("recipe_id", str),
                Arg("start", int, optional=True)
            ]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        if inputs['start'] is None:
            inputs['start'] = 0

        statement = (
            "SELECT Interactions.interactionID, rating, comment "
            "FROM Reviews INNER JOIN Interactions "
            "ON Reviews.interactionID = Interactions.InteractionID "
            "WHERE recipeID = %s "
            "LIMIT %s, 10 "
        )
        cursor.execute(
            statement,
            (inputs['recipe_id'], inputs['start'])
        )
        result = cursor.fetchall()
        if result:
            for r in result:
                print(r)
        else:
            print("No results found")

# search for a recipe
class SearchRecipeCommand(Command):
    def __init__(self):
        super().__init__(
            "Search recipe",
            [
                Arg("recipe_name", str, optional=True),
                Arg("author_name", str, optional=True),
                Arg("max_time",    str, optional=True),
                Arg("start",       int, optional=True)
            ]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return
        
        if inputs['start'] is None:
            inputs['start'] = 0

        search_criteria = []
        data = ()
        if inputs['recipe_name'] is not None:
            search_criteria.append("recipeName LIKE %s")
            data += (f"%{inputs['recipe_name']}%",)
        if inputs['author_name'] is not None:
            search_criteria.append("authorName = %s")
            data += (inputs['author_name'],)
        if inputs['max_time'] is not None:
            search_criteria.append("minutesToComplete < %s")
            data += (inputs['max_time'],)
        data += (inputs['start'],)

        statement = (
            "SELECT Recipes.recipeID, recipeName, AVG(rating) as avgRating, authorName, minutesToComplete "
            "FROM Recipes LEFT JOIN Reviews USING (recipeID) "
        )
        if list(inputs.values()).count(None) < len(inputs) - 1:
            statement += "WHERE "
            statement += " AND ".join(search_criteria)
        statement += " GROUP BY Reviews.recipeID ORDER BY avgRating DESC LIMIT %s,10"
        cursor.execute(statement, data)
        results = cursor.fetchall()
        print(f"{cursor.rowcount} result(s) found")
        if results:
            for r in results:
                print(r)
        else:
            print("No results found")

        # cursor.execute("DROP TABLE IF EXISTS Target")
        # statement = "CREATE TABLE Target AS " + statement
        # connection.commit()

        
# search for a recipe by ingredient
class SearchRecipeIngredientCommand(Command):
    def __init__(self):
        super().__init__(
            "Search Recipe by Ingredient",
            [
                Arg("ingredient_name", str),
                Arg("start", int, optional=True)
            ]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        if inputs['start'] is None:
            inputs['start'] = 0

        # update view
        statement = (
            "SELECT Recipes.recipeID, recipeName "
            "FROM Ingredients " 
            "INNER JOIN Uses "
            "ON Uses.ingredientID = Ingredients.ingredientID "
            "INNER JOIN Recipes "
            "ON Uses.recipeID = Recipes.recipeID "
            "WHERE ingredientName = %s "
            "LIMIT %s, 10"
        )
        data = (inputs['ingredient_name'], inputs['start'])
        cursor.execute(statement, data)
        result = cursor.fetchall()
        if result:
            for r in result:
                print(r)
        else:
            print("No results found")

class ShowRecipeDetails(Command):
    def __init__(self):
        super().__init__(
            "Show recipe details",
            [
                Arg("recipe_id", str),
            ]
        )
    
    def run(self):
        # get inputs
        inputs = self.get_inputs()
        if not inputs:
            return

        statement = (
            "SELECT * "
            "FROM Recipes " 
            "WHERE recipeID = %s"
        )
        data = (inputs['recipe_id'],)
        cursor.execute(statement, data)
        result = cursor.fetchone()
        if result:
            print(result)
        else:
            print("No results found")
        
        statement = (
            "SELECT ingredientName "
            "FROM Uses INNER JOIN Ingredients USING (ingredientID) "
            "WHERE recipeID = %s"
        )
        cursor.execute(statement, data)
        result = cursor.fetchall()
        if result:
            print(result)
        else:
            print("No ingredients found")
# # search for a recipe by author
# class SearchRecipeAuthorCommand(Command):
#     def __init__(self):
#         super().__init__(
#             "Search Recipe by Author",
#             [
#                 Arg("author_name", str)
#             ]
#         )
    
#     def run(self):
#         # get inputs
#         inputs = self.get_inputs()
#         if not inputs:
#             return

#         # update view
#         statement = (
#             "ALTER TABLE Target AS"
#             "SELECT recipeID"
#             "FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID)"
#             "WHERE authorName LIKE %s"
#         )
#         data = (inputs['author_name'])
#         cursor.execute(statement, data)
#         target = cursor.fetchall()
#         print(f"{len(target)} row(s) found")
        
# # search for a recipe by max cooking time
# class SearchRecipeTimeCommand(Command):
#     def __init__(self):
#         super().__init__(
#             "Search Recipe by Max Cooking Time",
#             [Arg("time", int)]
#         )
    
#     def run(self):
#         # get inputs
#         inputs = self.get_inputs()
#         if not inputs:
#             return

#         # update view
#         statement = (
#             "ALTER TABLE Target AS"
#             "SELECT recipeID"
#             "FROM (Target INNER JOIN Recipes ON Target.recipeID = Recipes.recipeID)"
#             "WHERE minutesToComplete < %d"
#         )
#         data = (inputs['author_name'])
#         cursor.execute(statement, data)
#         target = cursor.fetchall()
#         print(f"{len(target)} row(s) found")
        
# # sort recipes by # reviews
# class SortRecipeReviewsCommand(Command):
#     def __init__(self):
#         super().__init__(
#             "Search Recipe by Author",
#             [Arg("sort_order", str)]
#         )
    
#     def run(self):
#         # get inputs
#         inputs = self.get_inputs()
#         if not inputs:
#             return

#         # update view
#         statement = (
#             "ALTER TABLE Target AS "
#             "SELECT recipeID, COUNT(recipeID) AS reviewCount "
#             "FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID "
#             "GROUP BY recipeID "
#             "ORDER BY reviewCount %s"
#         )
#         data = (inputs['sort_order'])
#         cursor.execute(statement, data)
#         print("results sorted")

# # sort recipes by average rating
# class SortRecipeRatingCommand(Command):
#     def __init__(self):
#         super().__init__(
#             "Search Recipe by Author",
#             [Arg("sort_order", str)]
#         )
    
#     def run(self):
#         # get inputs
#         inputs = self.get_inputs()
#         if not inputs:
#             return

#         # update view
#         statement = (
#             "ALTER TABLE Target AS "
#             "SELECT recipeID, AVG(rating) AS avgRating "
#             "FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID "
#             "GROUP BY recipeID "
#             "ORDER BY avgRating %s "
#         )
#         data = (inputs['sort_order'])
#         cursor.execute(statement, data)
#         print("results sorted")
        
# # clear all search criterion
# class ClearSearchCriterionCommand(Command): 
#     def __init__(self):
#         super().__init__("Clear All Search Criterion", [])
    
#     def run(self):
#         # update view
#         cursor.execute("DROP TABLE IF EXISTS Target")
#         cursor.execute("CREATE TABLE Target AS SELECT recipeID FROM Recipes")
#         connection.commit()
#         print("search criterion cleared")
    

# # list search results
# class ListSearchResultsCommand(Command):
#     def __init__(self):
#         self.name = "List Search Results"

#         global initialized
#         if not initialized: 
#             # initialize 
#             statement = (
#                 "SELECT Target.recipeID, recipeName, minutesToComplete, AVG(rating) AS avgRating"
#                 "FROM (Recipes INNER JOIN Target ON Recipes.recipeID = Target.recipeID)"
#                 "INNER JOIN Reviews ON Recipes.recipeID = Reviews.recipeID"
#             )
#             cursor.execute(statement)
#             results = cursor.fetchall()
#             initialized = True

#     def run(self):
#         # list search results from the current index
#         finish = max(len(results), index + results_per_page)
#         for row in range(index, finish):
#             print_overview(row)
        

# # list results next page
# class SearchResultsNextPageCommand(Command):
#     def __init__(self):
#         super().__init__("Search Results Next Page", [])
#         global initialized
#         if not initialized: 
#             # initialize 
#             statement = (
#                 "SELECT recipeID, recipeName, minutesToComplete, AVG(rating) AS avgRating"
#                 "FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID"
#             )
#             cursor.execute(statement)
#             results = cursor.fetchall()
#             initialized = True

#     def run(self):
#         global index
#         if index + results_per_page < len(results):
#             index = index + results_per_page
#             finish = max(len(results), index + results_per_page)
#             for row in range(index, finish):
#                 print_overview(row)
#         else: 
#             print("reached end of results")

# # list results previous page
# class SearchResultsPreviousPageCommand(Command):
#     def __init__(self):
#         super().__init__("Search Results Previous Page", [])
#         global initialized
#         global cursor
#         if not initialized: 
#             # initialize 
#             statement = (
#                 "SELECT recipeID, recipeName, minutesToComplete, AVG(rating) AS avgRating"
#                 "FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID"
#             )
#             cursor.execute(statement)
#             results = cursor.fetchall()
#             initialized = True

#     def run(self):
#         global index
#         if index - results_per_page > 0:
#             finish = index
#             index = index - results_per_page
#             for row in range(index, finish):
#                 print_overview(row)
#         else: 
#             print("reached beginning of results")
        
# # display recipe details
# class DisplayRecipeDetailsCommand(Command):
#     def __init__(self):
#         super().__init__(
#             "Display Recipe Details",
#             [Arg("recipe_id, int")]
#         )
        
#     def run(self):
#         # get inputs
#         inputs = self.get_inputs()
#         if not inputs:
#             return

#         # Display details
#         statement = (
#             "SELECT *"
#             "FROM Target INNER JOIN Reviews ON Target.recipeID = Reviews.recipeID"
#             "WHERE recipeID = %s"
#         )
#         data = (inputs['recipe_id'])
#         cursor.execute(statement, data)
#         target_row = cursor.fetchall()
#         if len(target_row) != 1: 
#             print("Error: invalid recipeID")
#             raise InvalidInputError
#         else: 
#             print_details(target_row)
    # def run(self):
    #     # get inputs
    #     inputs = self.get_inputs()
    #     if not inputs:
    #         return

    #     # Display details
    #     statement = (
    #         "SELECT Recipes.recipeName, Recipes.authorName, Recipes.minutesToComplete, Recipes.dateSubmitted, Recipes.photo, Recipes.directions, AVG(rating) AS avgRating "
    #         "FROM Recipes INNER JOIN Target ON Recipes.recipeID = Target.recipeID INNER JOIN Reviews ON Recipes.recipeID = Reviews.recipeID"
    #         "WHERE recipeID = %s"
    #     )
    #     data = (inputs['recipe_id'])
    #     cursor.execute(statement, data)
    #     target_row = cursor.fetchall()
    #     if len(target_row) != 1: 
    #         print("Error: invalid recipeID")
    #         raise InvalidInputError
    #     else: 
    #         print_details(target_row[0])

def print_overview(t):
    print(f"id: {t[0]}")
    print(f"name: {t[1]}")
    print(f"time required: {t[2]}")
    print()

def print_details(t):
    print(f"name: {t[0]}")
    print(f"recipe by: {t[1]}")
    print(f"cooking time: {t[2]}")
    print(f"rating: {t[6]}")
    print(f"submitted at: {t[3]}")
    print(f"photo URL: {t[4]}")
    print(f"directions: {t[5]}")
    print()

# initialize all commands with proper arguments 
class Commands():
    def __init__(self):
        self.connection = None
        self.commands = [
            QuitCommand(),
            CreateRecipeCommand(),
            DeleteRecipeCommand(),
            EditRecipeCommand(),
            CreateReviewCommand(),
            DeleteReviewCommand(),
            EditReviewCommand(),
            ShowReviewsCommand(),
            SearchRecipeCommand(),
            # SortRecipeCommand(),
            # DisplayRecipeDetailsCommand(),
            # ClearSearchCriterionCommand()
            SearchRecipeIngredientCommand(),
            ShowRecipeDetails()
            # SearchRecipeAuthorCommand(),
            # SearchRecipeTimeCommand(),
            # SortRecipeReviewsCommand(),
            # ListSearchResultsCommand(),
            # SearchResultsNextPageCommand(),
            # SearchResultsPreviousPageCommand(),
            # DisplayRecipeDetailsCommand()
        ]