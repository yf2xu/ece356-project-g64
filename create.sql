-- Entity Tables:

-- > Users
CREATE TABLE Users(
    userID int auto_increment,
    primary key (userID)
);

-- > Interactions
CREATE TABLE Interactions(
    interactionID   int auto_increment,
    comment        text,
    primary key (interactionID)
);

-- > Recipes
CREATE TABLE Recipes(
    recipeID            int auto_increment,
    recipeName          varchar(255)    not null,
    authorName          varchar(100),
    minutesToComplete   decimal(5, 0)   check(minutesToComplete > 0),
    dateSubmitted       datetime,
    photo               varchar(255)    check(photo RLIKE '^https?:\/\/.*\.(?:png|jpg)$'),
    directions          text    not null,
    index (minutesToComplete),
    primary key (recipeID)
);

-- > Ingredients
CREATE TABLE Ingredients(
    ingredientID    int auto_increment,
    ingredientName  varchar(255)  unique,
    primary key (ingredientID)
);

-- Relationship Tables:

-- > Reviews
CREATE TABLE Reviews(
    interactionID   int,
    recipeID        int,
    rating          decimal (1,0) check(rating between 1 and 5)   not null,
    primary key (interactionID, recipeID),
    foreign key (interactionID) references Interactions(interactionID) ON DELETE CASCADE,
    foreign key (recipeID) references Recipes(recipeID) ON DELETE CASCADE
);

-- > Writes
CREATE TABLE Writes(
    userID int,
    interactionID int,
    primary key (userID, interactionID),
    foreign key (userID) references Users(userID) ON DELETE CASCADE,
    foreign key (interactionID) references Interactions(interactionID) ON DELETE CASCADE
);

-- > Uses
CREATE TABLE Uses(
    recipeID int,
    ingredientID int,
    primary key (recipeID, ingredientID),
    foreign key (recipeID) references Recipes(recipeID) ON DELETE CASCADE,
    foreign key (ingredientID) references Ingredients(ingredientID) ON DELETE CASCADE
);
