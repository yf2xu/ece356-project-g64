from commands import Commands

cmds = Commands()

def print_line():
    print("----------------------------")

def print_error():
    print("Error: invalid input")

def show_menu():
    print_line()
    for i, cmd in enumerate(cmds.commands):
        print(f"{i} - {cmd.name}")
    print_line()

if __name__ == "__main__":
    while True:
        show_menu()
        opt = input("Enter option number: ")

        if not opt.isdigit():
            print_error()
        elif 0 <= int(opt) < len(cmds.commands):
            cmds.commands[int(opt)].run()
        else:
            print_error()
